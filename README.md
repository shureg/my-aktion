# JavaEE7 Workshop

Webanwendung aus dem Buch Workshop JavaEE7 von Marcus Schießer & Martin Schmöllinger, 2. Auflage

Features, die nicht im Buch besprochen wurden:

+ Verwendung von [Lombok](https://projectlombok.org/)



Zum Buch existieren auch offiziele Sourcen:

+ Sourcen für die [erste Auflage](http://amzn.to/2iw4yz3) des Buchen [my-aktion](https://github.com/marcusschiesser/my-aktion)

+ Sourcen für die [zweite Auflage](http://amzn.to/2hP1bTc) des Buchen [my-aktion-2nd](https://github.com/marcusschiesser/my-aktion-2nd)

Die Lektüre gibts auch in Englisch unter [turngeek.press](http://www.turngeek.press/)