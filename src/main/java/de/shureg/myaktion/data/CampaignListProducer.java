package de.shureg.myaktion.data;

import de.shureg.myaktion.model.Campaign;
import de.shureg.myaktion.services.CampaignService;
import de.shureg.myaktion.util.Events;

import javax.annotation.PostConstruct;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Alex on 23.12.2016.
 */
@RequestScoped
public class CampaignListProducer {

    private List<Campaign> campaigns;

    @Inject
    private CampaignService campaignService;

    @PostConstruct
    public void init() {
        this.campaigns = campaignService.getAllCampaigns();
    }

    @Produces
    @Named
    public List<Campaign> getCampaigns() {
        return campaigns;
    }

    public void onCampaignAdded(@Observes @Events.Added Campaign campaign) {
        campaignService.addCampaign(campaign);
        init();
    }

    public void onCampaignDeleted(@Observes @Events.Deleted Campaign campaign) {
        campaignService.deleteCampaign(campaign);
        init();
    }

    public void onCampaignUpdated(@Observes @Events.Updated Campaign campaign) {
        campaignService.updateCampaign(campaign);
        init();
    }


}
