package de.shureg.myaktion.data;

import de.shureg.myaktion.model.Campaign;

import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Alex on 26.12.2016.
 */
@SessionScoped
public class CampaignProducer implements Serializable {
    private static final long serialVersionUID = -4423830067480514709L;

    private enum Mode{
        EDIT, ADD
    }

    private Campaign campaign;
    private Mode mode;

    @Produces
    @Named
    public Campaign getSelectedCampaign() {
        return campaign;
    }

    public void setSelectedCampaign(Campaign campaign) {
        this.campaign = campaign;
    }

    @Produces
    @Named
    public boolean isAddMode(){
        return mode == Mode.ADD;
    }

    public void prepareAddCampaign(){
        this.campaign = new Campaign();
        this.mode = Mode.ADD;
    }

    public void prepareEditCampaign(Campaign campaign){
        this.campaign = campaign;
        this.mode = Mode.EDIT;
    }
}
