package de.shureg.myaktion.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Alex on 19.12.2016.
 */
@NamedQueries({
        @NamedQuery(name=Campaign.findAll, query = "SELECT c FROM Campaign c ORDER BY c.name"),
        @NamedQuery(name=Campaign.getAmountDonatedSoFar, query = "SELECT SUM(d.amount) FROM Donation d where d.campaign=:campaign")
})
@Getter
@Setter
@Entity
public class Campaign {

    public static  final String findAll = "Campaign.findAll";
    public static  final String getAmountDonatedSoFar = "Campaign.getAmountDonatedSoFar";

    @GeneratedValue
    @Id
    private Long id;
    private String name;
    private Double targetAmount;
    private Double donationMinimum;
    @Transient
    private Double amountDonatedSoFar;

    @Embedded
    @AttributeOverrides({@AttributeOverride(name = "name", column = @Column(name = "accountName"))})
    private Account account;

    @OneToMany(mappedBy = "campaign")
    private List<Donation> donations;

    /**
     * Default constructor
     */
    public Campaign() {
        this.account = new Account();
    }
}
