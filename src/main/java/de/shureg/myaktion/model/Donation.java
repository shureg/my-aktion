package de.shureg.myaktion.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by Alex on 19.12.2016.
 */
@Getter
@Setter
@Entity
public class Donation {

    @GeneratedValue
    @Id
    private Long id;
    private Double amount;
    private String donorName;
    private Boolean receiptRequested;
    private Status status;

    @Embedded
    private Account account;

    @ManyToOne
    private Campaign campaign;

    /* Status Liste*/
    public enum Status{TRANSFERRED, IN_PROCESS;}

    /**
     * Default Constuctor
     */
    public Donation(){
        this.account = new Account();
    }

}
