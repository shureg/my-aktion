package de.shureg.myaktion.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Alex on 29.12.2016.
 */
@Getter
@Setter
public class FormConfig {

    private String textColor = "000000";
    private String bgColor = "ffffff";
    private String title = "Geld spenden";
}
