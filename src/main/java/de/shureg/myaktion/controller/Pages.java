package de.shureg.myaktion.controller;

/**
 * Created by Alex on 23.12.2016.
 */
public class Pages {
    public static final String LIST_CAMPAIGNS = "listCampaigns";
    public static final String EDIT_CAMPAIGN = "editCampaign";
    public static final String EDIT_DONATION_FORM = "editDonationForm";
    public static final String LIST_DONATIONS = "listDonations";
    public static final String DONATE_MONEY = "donateMoney";
}
