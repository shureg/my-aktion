package de.shureg.myaktion.controller;

import de.shureg.myaktion.model.Donation;
import de.shureg.myaktion.services.DonationService;
import lombok.Getter;
import lombok.Setter;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Alex on 28.12.2016.
 */
@ViewScoped
@Named
@Getter
@Setter
public class DonateMoneyController implements Serializable {
    private static final long serialVersionUID = 1542538776783071081L;

    private String textColor = "000000";
    private String bgColor = "ffffff";
    private String title;
    private Long campaignId;
    private Donation donation;

    @Inject
    private DonationService donationService;

    @Inject
    private FacesContext facesContext;
    @Inject
    private Logger logger;

    @PostConstruct
    public void init() {
        this.donation = new Donation();
    }

    public String doDonation(){
        getDonation().setStatus(Donation.Status.IN_PROCESS);
        donationService.addDonation(getCampaignId(),getDonation());
        logger.log(Level.INFO,"log.donateMoney.thank_you",new Object[]{getDonation().getDonorName(),getDonation().getAmount()});
        final ResourceBundle resourceBundle = facesContext.getApplication().getResourceBundle(facesContext,"msg");
        final String msg =  resourceBundle.getString("donateMoney.thank_you");
        facesContext.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_INFO,msg,null));
        init();
        return Pages.DONATE_MONEY;
    }
}
