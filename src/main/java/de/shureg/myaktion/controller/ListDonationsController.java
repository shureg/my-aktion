package de.shureg.myaktion.controller;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Alex on 27.12.2016.
 */
@ViewScoped
@Named
public class ListDonationsController implements Serializable {

    private static final long serialVersionUID = -2588774123296327011L;


    /**
     *
     * @return
     */
    public  String doOk() {
        return Pages.LIST_CAMPAIGNS;
    }
}
