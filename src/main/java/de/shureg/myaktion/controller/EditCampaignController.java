package de.shureg.myaktion.controller;

import de.shureg.myaktion.data.CampaignListProducer;
import de.shureg.myaktion.data.CampaignProducer;
import de.shureg.myaktion.model.Campaign;
import de.shureg.myaktion.util.Events;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Alex on 27.12.2016.
 */
@ViewScoped
@Named
public class EditCampaignController implements Serializable{

    private static final long serialVersionUID = 4179866297166050696L;

    @Inject
    private CampaignProducer campaignProducer;

    @Inject
    @Events.Added
    private Event<Campaign> campaignAddEvent;
    @Inject
    @Events.Updated
    private Event<Campaign> campaignUpdateEvent;

    /**
     *
     * @return
     */
    public String doSave(){
        if(campaignProducer.isAddMode()){
            campaignAddEvent.fire(campaignProducer.getSelectedCampaign());
        } else{
            campaignUpdateEvent.fire(campaignProducer.getSelectedCampaign());
        }

        return Pages.LIST_CAMPAIGNS;
    }


    /**
     *
     * @return
     */
    public String doCancel(){
        return Pages.LIST_CAMPAIGNS;
    }
}
