package de.shureg.myaktion.controller;

import de.shureg.myaktion.data.CampaignProducer;
import de.shureg.myaktion.model.Campaign;
import de.shureg.myaktion.model.Donation;
import de.shureg.myaktion.services.DonationService;
import de.shureg.myaktion.util.Events;

import javax.enterprise.event.Event;
import javax.faces.view.ViewScoped;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Alex on 23.12.2016.
 */
@ViewScoped
@Named
public class ListCampaignsController implements Serializable {

    private static final long serialVersionUID = 8693277373648025822L;

    @Inject
    private CampaignProducer campaignProducer;

    @Inject
    private DonationService donationService;

    private Campaign campaignToDelete;

    @Inject @Events.Deleted
    private Event<Campaign> campaignDeleteEvent;
    /**
     *
     * @return
     */
    public String doAddCampaign(){
//        System.out.println("Add Campaign");
        campaignProducer.prepareAddCampaign();
        return Pages.EDIT_CAMPAIGN;
    }

    /**
     *
     * @return
     */
    public String doEditCampaign(Campaign campaign){
//        System.out.println("Edit Campaign "+ campaign);
        campaignProducer.prepareEditCampaign(campaign);
        return Pages.EDIT_CAMPAIGN;
    }

    /**
     *
     * @param campaign
     * @return
     */
    public String doEditDonationForm(Campaign campaign){
//        System.out.println("Edit Donation Form of Campaign " + campaign);
        campaignProducer.setSelectedCampaign(campaign);
        return Pages.EDIT_DONATION_FORM;
    }

    public String doListDonations(Campaign campaign){
        final List<Donation> donations = donationService.getDonationList(campaign.getId());
        campaign.setDonations(donations);
        campaignProducer.setSelectedCampaign(campaign);
        return Pages.LIST_DONATIONS;
    }

    public void doDeleteCampaign(Campaign campaign){
        this.campaignToDelete = campaign;
        System.out.println("Delete Campaign " + campaign);
    }
    public void commitDeleteCampaign(){
        campaignDeleteEvent.fire(campaignToDelete);
        System.out.println("Delete Campaign ist noch nicht implementiert");
    }


}
