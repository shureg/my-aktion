package de.shureg.myaktion.controller;

import de.shureg.myaktion.data.CampaignProducer;
import de.shureg.myaktion.model.FormConfig;
import lombok.Getter;
import lombok.Setter;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created by Alex on 27.12.2016.
 */
@ViewScoped
@Named
public class EditDonationFormController implements Serializable {

    private static final long serialVersionUID = -3862552009341587384L;

    @Getter
    @Setter
    private FormConfig formConfig;

    @Inject
    private CampaignProducer campaignProducer;
    @Inject
    private HttpServletRequest req;

    public EditDonationFormController(){
        this.formConfig = new FormConfig();
    }
    public String doOk(){
        return Pages.LIST_CAMPAIGNS;
    }

    private String getAppUrl(){

        String scheme = req.getScheme();
        String serverName = req.getServerName();
        int port = req.getServerPort();
        String contextPath = req.getContextPath();

        return scheme + "://" + serverName + ":" + port + contextPath;

    }
    public String getUrl(){
        return getAppUrl() + "/" + Pages.DONATE_MONEY + ".jsf?bgColor=" + formConfig.getBgColor() +
                                                                "&textColor=" + formConfig.getTextColor() +
                                                                "&title=" + formConfig.getTitle() +
                                                                "&campaignId=" + campaignProducer.getSelectedCampaign().getId();

    }
}
