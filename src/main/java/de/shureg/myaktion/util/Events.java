package de.shureg.myaktion.util;

import javax.inject.Qualifier;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;

/**
 * Created by Alex on 03.01.2017.
 */
public class Events {

    @Qualifier
    @Target({FIELD,PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Added{}

    @Qualifier
    @Target({FIELD,PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Deleted{}

    @Qualifier
    @Target({FIELD,PARAMETER})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Updated{}

}
