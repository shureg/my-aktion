package de.shureg.myaktion.services;

import de.shureg.myaktion.model.Campaign;
import de.shureg.myaktion.model.Donation;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by Alex on 13.01.2017.
 */
@Stateless
public class DonationServiceBean implements DonationService {

    @Inject
    private EntityManager entityManager;

    /**
     *
     * @param campaignId
     * @return
     */
    public List<Donation> getDonationList(Long campaignId) {
        Campaign managedCampaign = entityManager.find(Campaign.class,campaignId);
        List<Donation> donations = managedCampaign.getDonations();
        donations.size();
        return donations;

    }

    /**
     *
     * @param campaignId
     * @param donation
     */
    public void addDonation(Long campaignId, Donation donation) {
        Campaign managedCampaign = entityManager.find(Campaign.class,campaignId);
        donation.setCampaign(managedCampaign);
        entityManager.persist(donation);
    }
}
