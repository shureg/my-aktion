package de.shureg.myaktion.services;

import de.shureg.myaktion.model.Campaign;

import java.util.List;

/**
 * Created by Alex on 03.01.2017.
 */
public interface CampaignService {

    List<Campaign> getAllCampaigns();
    void addCampaign(Campaign campaign);
    void updateCampaign(Campaign campaign);
    void deleteCampaign(Campaign campaign);
}
