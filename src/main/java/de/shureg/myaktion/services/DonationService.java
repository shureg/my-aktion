package de.shureg.myaktion.services;

import de.shureg.myaktion.model.Donation;

import java.util.List;

/**
 * Created by Alex on 13.01.2017.
 */
public interface DonationService {

    List<Donation> getDonationList(Long campaignId);
    void addDonation(Long campaignId, Donation donation);
}
