package de.shureg.myaktion.test;


import de.shureg.myaktion.model.Campaign;
import de.shureg.myaktion.test.pages.EditCampaignPage;
import de.shureg.myaktion.test.pages.ListCampaignsPage;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.graphene.page.InitialPage;
import org.jboss.arquillian.graphene.page.Page;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
/**
 * Created by Alex on 29.12.2016.
 */
@RunWith(Arquillian.class)
public class EditCampaignITCase extends AbstractITCase{
    @Drone
    private WebDriver browser;
    @Page
    private EditCampaignPage editCampaignPage;

    @Test
    public void testAddCampaign(@InitialPage ListCampaignsPage listCampaignsPage){
        final Campaign testCampaign = DataFactory.createTestCampaign();
        listCampaignsPage.doAddCampaign();
        editCampaignPage.assertOnPage();

        editCampaignPage.setCampaign(testCampaign);
        editCampaignPage.doSave();
        listCampaignsPage.assertOnPage();
        listCampaignsPage.assertCampaignName(testCampaign.getName());
    }

}
