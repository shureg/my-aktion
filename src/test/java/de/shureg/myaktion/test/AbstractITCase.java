package de.shureg.myaktion.test;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import java.io.File;

/**
 * Created by Alex on 29.12.2016.
 */

public abstract class AbstractITCase {
    @Deployment(testable=false)
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(ZipImporter.class, "test.war")
                .importFrom(new File("target/my-aktion.war"))
                .as(WebArchive.class);
    }
}
